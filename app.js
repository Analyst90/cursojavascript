let html;
let nombre = "Williams";
let apellido = "Paez";
let fechaNac = new Date(1990,12,24);

//templates literal-------------------------------------------------
html = `
        <ul>
            <li>Nombre: ${nombre}</li>
            <li>Apellido: ${apellido}</li>
            <li>Fecha de nacimiento: ${fechaNac}</li>
        </ul>
        `;
//------------------------------------------------------------------        

document.getElementById("app").innerHTML = html;

//objetos ----------------------------------------
let persona = {
    nombreP: "Pepito",
    apellidoP: "Perez",
    edad: 20,
    nacimiento: function(){return new Date().getFullYear - this.edad},
    //objeto dentro de objeto
    residencia : {ciudad: "Rio Cuarto", provincia: "Córdoba"}
}

console.log(persona);
console.log(persona.residencia);
//------------------------------------------------------------------------